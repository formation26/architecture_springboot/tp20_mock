package fr.epita.tp20.mock.service;

import java.util.List;

import fr.epita.tp20.mock.domaine.Employe;

public interface EmployeService {
	
	void create(Employe e);
	
	List<Employe> findAll();

}
