package fr.epita.tp20.mock.service;

import java.math.BigDecimal;
import java.util.List;

import fr.epita.tp20.mock.domaine.Employe;
import fr.epita.tp20.mock.infra.EmployeDao;
import fr.epita.tp20.mock.infra.EmployeDaoImpl;

public class EmployeServiceImpl implements EmployeService {

	
	EmployeDao dao;
	
	 public EmployeServiceImpl(EmployeDao dao) {
		this.dao=dao;
	}
	
	
	@Override
	public void create(Employe e) {
		if(e.getNom().equals("TOTO")) {
			 throw new IllegalArgumentException();
		}
		
		if(e.getSalaire().compareTo(new BigDecimal(15000)) ==0 ) {
			throw new IllegalArgumentException();
		}
		
		
		dao.create(e);

	}

	@Override
	public List<Employe> findAll() {
		
		
		return dao.findAll();
	}

}
