package fr.epita.tp20.mock.infra;

import java.util.ArrayList;
import java.util.List;

import fr.epita.tp20.mock.domaine.Employe;

public class EmployeDaoImpl implements EmployeDao {

	@Override
	public void create(Employe e) {
		System.out.println("Je suis dans la couche INFRA----Lien avec la BDD");

	}

	@Override
	public List<Employe> findAll() {
		List<Employe> list=new ArrayList<Employe>();
		
		Employe e=new Employe();
		e.setNom("EmployeFROM BDD");
		
		list.add(e);
		return list;
	}

}
