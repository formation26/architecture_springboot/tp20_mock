package fr.epita.tp20.mock.infra;

import java.util.List;

import fr.epita.tp20.mock.domaine.Employe;

public interface EmployeDao {
	void create(Employe e);
	
	List<Employe> findAll();
}
