package fr.epita.tp20.mock.domaine;

import java.math.BigDecimal;

public class Employe {
	
	private String nom;
	
	private BigDecimal salaire;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public BigDecimal getSalaire() {
		return salaire;
	}

	public void setSalaire(BigDecimal salaire) {
		this.salaire = salaire;
	}
	
	

}
