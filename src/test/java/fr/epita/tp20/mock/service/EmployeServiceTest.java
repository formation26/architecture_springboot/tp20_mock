package fr.epita.tp20.mock.service;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.epita.tp20.mock.domaine.Employe;
import fr.epita.tp20.mock.infra.EmployeDao;
 

@ExtendWith(MockitoExtension.class)
public class EmployeServiceTest {
	
	List<Employe> mockList;
	
	
	@Mock
	EmployeDao dao ;
	
	Employe employeA;
	
	@InjectMocks
	EmployeService service = new EmployeServiceImpl(dao);
	
	@BeforeEach
	public void setUp() {
		
		//service = new EmployeServiceImpl();
		mockList=new ArrayList<Employe>();
		Employe e=new Employe();
		e.setNom("durand");
		e.setSalaire(new BigDecimal(24000));
		mockList.add(e);
		
		employeA=new Employe();
		employeA.setNom("TOTO");
		employeA.setSalaire(new BigDecimal(15000));
	}
	
	@Test
	public void create() {
		Mockito.doNothing().when(dao).create(employeA);
		assertThrows(IllegalArgumentException.class, ()-> service.create(employeA));
		assertThrows(IllegalArgumentException.class, ()-> service.create(employeA));
		
		service.create(employeA);
		
				
	}
	
	
	@Test
	public void getAll() {
		Mockito.when(dao.findAll()).thenReturn(mockList);
		List<Employe> list=service.findAll();
		
		for(Employe e:list) {
			
			System.out.println(e.getNom());
		}
		
	}

}
